package ru.edu.eduscheduler.services;

import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import ru.edu.eduscheduler.model.Subject;
import ru.edu.eduscheduler.repository.SubjectRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class SubjectServiceTest {

	@Mock
	private SubjectRepository repository;

	private static Subject subject;

	@BeforeAll
	static void initialize() {
		subject = new Subject();
		subject.setId(1L);
	}

	@Test
	public void findById() {
		Mockito.when(repository.findById(Mockito.isA(Long.class))).thenReturn(Optional.of(subject));
		SubjectService service = new SubjectService(repository);
		Assertions.assertEquals(subject, service.findById(subject.getId()));
	}

	@Test
	public void getListAvailableForGroup() {
		Mockito.when(repository.getListAvailableForGroup(Mockito.isA(Long.class))).thenReturn(Set.of(subject));
		SubjectService service = new SubjectService(repository);
		Assertions.assertTrue(service.getListAvailableForGroup(1L).contains(subject));
	}

}
