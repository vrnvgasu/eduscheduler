package ru.edu.eduscheduler.services;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import ru.edu.eduscheduler.dto.TeacherDTO;
import ru.edu.eduscheduler.model.Teacher;
import ru.edu.eduscheduler.model.User;
import ru.edu.eduscheduler.repository.TeacherRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class TeacherServiceTest {

	private static final int PAGE_SIZE = 5;

	@Mock
	private TeacherRepository repository;

	private static Teacher teacher;

	@BeforeAll
	static void initialize() {
		teacher = new Teacher();
		teacher.setId(1L);
		teacher.setUser(new User());
	}

	@Test
	public void findById() {
		Mockito.when(repository.findById(Mockito.isA(Long.class))).thenReturn(Optional.of(teacher));
		TeacherService service = new TeacherService(repository);
		Assertions.assertEquals(teacher, service.findById(teacher.getId()));
	}

	@Test
	public void getDTOListPagination() {
		Mockito.when(repository.findAll(Mockito.isA(Pageable.class)))
				.thenReturn(new PageImpl<>(
						List.of(teacher),
						PageRequest.of(0, PAGE_SIZE),
						1
				));
		TeacherService service = new TeacherService(repository);
		Page<TeacherDTO> teacherDTOPage = service.getDTOListPagination(Optional.of(1));
		Assertions.assertEquals(1, teacherDTOPage.getTotalPages());
		Assertions.assertEquals(1, teacherDTOPage.getTotalElements());
	}

}
