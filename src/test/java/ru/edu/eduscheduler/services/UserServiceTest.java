package ru.edu.eduscheduler.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.edu.eduscheduler.dto.UserFormDTO;
import ru.edu.eduscheduler.model.User;
import ru.edu.eduscheduler.repository.UserRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class UserServiceTest {

	private static final String EMAIL = "test@test.com";

	@Mock
	private UserRepository repository;

	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	@Mock
	private JavaMailSender javaMailSender;

	private static User user;

	@BeforeAll
	static void initialize() {
		user = new User();
		user.setEmail(EMAIL);
	}

	@Test
	public void getUserByEmail() {
		Mockito.when(repository.findUserByEmail(Mockito.isA(String.class))).thenReturn(user);
		UserService service = new UserService(repository, bCryptPasswordEncoder, javaMailSender);
		Assertions.assertEquals(user, service.getUserByEmail(user.getEmail()));
	}

	@Test
	public void createFromDTO() {
		Mockito.when(repository.save(Mockito.isA(User.class))).thenReturn(user);
		UserService service = new UserService(repository, bCryptPasswordEncoder, javaMailSender);
		UserFormDTO userFormDTO = new UserFormDTO();
		userFormDTO.setPassword("password");
		service.createFromDTO(userFormDTO);
	}

}
