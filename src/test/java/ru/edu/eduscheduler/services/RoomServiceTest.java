package ru.edu.eduscheduler.services;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import ru.edu.eduscheduler.dto.RoomDTO;
import ru.edu.eduscheduler.model.Room;
import ru.edu.eduscheduler.repository.RoomRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class RoomServiceTest {

	private static final int PAGE_SIZE = 5;

	@Mock
	private RoomRepository repository;

	private static Room room;

	@BeforeAll
	static void initialize() {
		room = new Room();
		room.setId(1L);
	}

	@Test
	public void findById() {
		Mockito.when(repository.findById(Mockito.isA(Long.class))).thenReturn(Optional.of(room));
		RoomService service = new RoomService(repository);
		Assertions.assertEquals(room, service.findById(room.getId()));
	}

	@Test
	public void getDTOListPagination() {
		Mockito.when(repository.findAll(Mockito.isA(Pageable.class)))
				.thenReturn(new PageImpl<>(
						List.of(room),
						PageRequest.of(0, PAGE_SIZE),
						1
				));
		RoomService service = new RoomService(repository);
		Page<RoomDTO> groupDTOPage = service.getDTOListPagination(Optional.of(1));
		Assertions.assertEquals(1, groupDTOPage.getTotalPages());
		Assertions.assertEquals(1, groupDTOPage.getTotalElements());
	}

	@Test
	public void getDTOList() {
		Mockito.when(repository.findAll()).thenReturn(List.of(room));
		RoomService service = new RoomService(repository);
		Assertions.assertEquals(1, service.getDTOList().size());
		Assertions.assertEquals(room.getId(), service.getDTOList().get(0).getId());
	}

}
