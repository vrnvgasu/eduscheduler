package ru.edu.eduscheduler.services;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import ru.edu.eduscheduler.dto.DateEducationPlanLinkedMapDTO;
import ru.edu.eduscheduler.model.EducationPlan;
import ru.edu.eduscheduler.model.Group;
import ru.edu.eduscheduler.model.Room;
import ru.edu.eduscheduler.model.Schedule;
import ru.edu.eduscheduler.model.Subject;
import ru.edu.eduscheduler.model.Teacher;
import ru.edu.eduscheduler.model.User;
import ru.edu.eduscheduler.repository.EducationPlanRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class EducationPlanServiceTest {

	@Mock
	private EducationPlanRepository repository;

	@Mock
	private ScheduleService scheduleService;

	@Mock
	private RoomService roomService;

	@Mock
	private GroupService groupService;

	@Mock
	private SubjectService subjectService;

	@Mock
	private TeacherService teacherService;

	private static EducationPlan educationPlan;

	@BeforeAll
	static void initialize() {
		educationPlan = new EducationPlan();
		educationPlan.setId(1L);
		educationPlan.setDate(LocalDate.now());

		Schedule schedule = new Schedule();
		schedule.setId(1L);
		schedule.setOrder(1);
		educationPlan.setSchedule(schedule);

		Teacher teacher = new Teacher();
		User user = new User();
		teacher.setUser(user);
		educationPlan.setTeacher(teacher);

		educationPlan.setRoom(new Room());
		educationPlan.setGroup(new Group());
		educationPlan.setSubject(new Subject());
	}

	@Test
	public void findById() {
		Mockito.when(repository.findById(Mockito.isA(Long.class))).thenReturn(Optional.of(educationPlan));
		EducationPlanService service = new EducationPlanService(
				repository,
				scheduleService,
				roomService,
				groupService,
				subjectService,
				teacherService
		);
		Assertions.assertEquals(educationPlan, service.findById(educationPlan.getId()));
	}

	@Test
	public void getEducationPlanLinkedMap() {
		Mockito.when(scheduleService.getAllOrdered()).thenReturn(Set.of(educationPlan.getSchedule()));

		Set<EducationPlan> educationPlanSet = Set.of(educationPlan);
		EducationPlanService service = new EducationPlanService(
				repository,
				scheduleService,
				roomService,
				groupService,
				subjectService,
				teacherService
		);
		DateEducationPlanLinkedMapDTO map = service.getEducationPlanLinkedMap(
				educationPlanSet,
				LocalDate.now().minusDays(2)
		);

		Assertions.assertTrue(map.getLinkedMapDTO().containsKey(LocalDate.now()));
		Assertions.assertEquals(7, map.getLinkedMapDTO().size());
		Assertions.assertEquals(1, map.getLinkedMapDTO().get(LocalDate.now()).getLinkedMap().size());
	}

}
