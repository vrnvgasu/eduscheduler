package ru.edu.eduscheduler.services;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import ru.edu.eduscheduler.dto.GroupDTO;
import ru.edu.eduscheduler.model.Group;
import ru.edu.eduscheduler.repository.GroupRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class GroupServiceTest {

	private static final int PAGE_SIZE = 5;

	@Mock
	private GroupRepository repository;

	private static Group group;

	@BeforeAll
	static void initialize() {
		group = new Group();
		group.setId(1L);
	}

	@Test
	public void findById() {
		Mockito.when(repository.findById(Mockito.isA(Long.class))).thenReturn(Optional.of(group));
		GroupService service = new GroupService(repository);
		Assertions.assertEquals(group, service.findById(group.getId()));
	}

	@Test
	public void getDTOListPagination() {
		Mockito.when(repository.findAll(Mockito.isA(Pageable.class)))
				.thenReturn(new PageImpl<>(
						List.of(group),
						PageRequest.of(0, PAGE_SIZE),
						1
				));
		GroupService service = new GroupService(repository);
		Page<GroupDTO> groupDTOPage = service.getDTOListPagination(Optional.of(1));
		Assertions.assertEquals(1, groupDTOPage.getTotalPages());
		Assertions.assertEquals(1, groupDTOPage.getTotalElements());
	}

	@Test
	public void getDTOById() {
		Mockito.when(repository.findById(Mockito.isA(Long.class))).thenReturn(Optional.of(group));
		GroupService service = new GroupService(repository);
		Assertions.assertEquals(group.getId(), service.getDTOById(group.getId()).getId());
	}

}
