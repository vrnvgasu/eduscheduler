package ru.edu.eduscheduler.services;

import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import ru.edu.eduscheduler.model.Student;
import ru.edu.eduscheduler.repository.StudentRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class StudentServiceTest {

	@Mock
	private StudentRepository repository;

	private static Student student;

	@BeforeAll
	static void initialize() {
		student = new Student();
		student.setIdNumber(1L);
	}

	@Test
	public void findByIdNumber() {
		Mockito.when(repository.findByIdNumber(Mockito.isA(Long.class))).thenReturn(Optional.of(student));
		StudentService service = new StudentService(repository);
		Assertions.assertEquals(student, service.findByIdNumber(student.getIdNumber()).get());
	}

}
