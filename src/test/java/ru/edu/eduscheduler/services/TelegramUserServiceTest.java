package ru.edu.eduscheduler.services;

import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import ru.edu.eduscheduler.model.TelegramUser;
import ru.edu.eduscheduler.repository.TelegramUserRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class TelegramUserServiceTest {

	@Mock
	private TelegramUserRepository repository;

	private static TelegramUser telegramUser;

	@BeforeAll
	static void initialize() {
		telegramUser = new TelegramUser();
		telegramUser.setChatId(1L);
	}

	@Test
	public void findOrCreate() {
		Mockito.when(repository.findByChatId(Mockito.isA(Long.class))).thenReturn(Optional.of(telegramUser));
		TelegramUserService service = new TelegramUserService(repository);
		Assertions.assertEquals(telegramUser, service.findOrCreate(telegramUser.getChatId()));
	}

}
