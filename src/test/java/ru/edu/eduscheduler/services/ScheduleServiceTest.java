package ru.edu.eduscheduler.services;

import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import ru.edu.eduscheduler.model.Schedule;
import ru.edu.eduscheduler.repository.ScheduleRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ScheduleServiceTest {

	@Mock
	private ScheduleRepository repository;

	private static Schedule schedule;

	@BeforeAll
	static void initialize() {
		schedule = new Schedule();
		schedule.setId(1L);
		schedule.setOrder(1);
	}

	@Test
	public void findById() {
		Mockito.when(repository.findById(Mockito.isA(Long.class))).thenReturn(Optional.of(schedule));
		ScheduleService service = new ScheduleService(repository);
		Assertions.assertEquals(schedule, service.findById(schedule.getId()));
	}

	@Test
	public void getDTOById() {
		Mockito.when(repository.findById(Mockito.isA(Long.class))).thenReturn(Optional.of(schedule));
		ScheduleService service = new ScheduleService(repository);
		Assertions.assertEquals(schedule.getId(), service.getDTOById(schedule.getId()).getId());
	}

	@Test
	public void getAllOrdered() {
		Mockito.when(repository.findAllByOrderByOrder()).thenReturn(Set.of(schedule));
		ScheduleService service = new ScheduleService(repository);
		Assertions.assertTrue(service.getAllOrdered().contains(schedule));
	}

}
