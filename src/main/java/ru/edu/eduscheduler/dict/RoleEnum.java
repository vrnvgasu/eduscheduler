package ru.edu.eduscheduler.dict;

public enum RoleEnum {
	ROLE_STUDENT,
	ROLE_TEACHER,
	ROLE_MANAGER,
	ROLE_ADMIN,
	ROLE_GUEST
}
