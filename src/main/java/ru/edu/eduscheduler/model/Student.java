package ru.edu.eduscheduler.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "students")
@SequenceGenerator(name = "default_gen", sequenceName = "students_id_seq", allocationSize = 1)
public class Student extends GenericModel {

	@Column(name = "id_number", nullable = false)
	private long idNumber;

	@Column(name = "group_id", nullable = false, updatable = false, insertable = false)
	private long groupId;

	@Column(name = "user_id", nullable = false, updatable = false, insertable = false)
	private long userId;

	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	@JsonIgnore
	@ToString.Exclude
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "group_id", referencedColumnName = "id")
	@JsonIgnore
	@ToString.Exclude
	private Group group;

}
