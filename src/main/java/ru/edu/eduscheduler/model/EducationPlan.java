package ru.edu.eduscheduler.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
@ToString
@Entity
@Table(name = "education_plans")
@SequenceGenerator(name = "default_gen", sequenceName = "education_plans_id_seq", allocationSize = 1)
public class EducationPlan extends GenericModel {

	@Column(name = "date", nullable = false)
	private LocalDate date;

	@Column(name = "room_id", nullable = false, updatable = false, insertable = false)
	private long roomId;

	@Column(name = "schedule_id", nullable = false, updatable = false, insertable = false)
	private long scheduleId;

	@Column(name = "group_id", nullable = false, updatable = false, insertable = false)
	private long groupId;

	@Column(name = "subject_id", nullable = false, updatable = false, insertable = false)
	private long subjectId;

	@Column(name = "teacher_id", nullable = false, updatable = false, insertable = false)
	private long teacherId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "room_id", referencedColumnName = "id")
	@JsonIgnore
	@ToString.Exclude
	private Room room;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schedule_id", referencedColumnName = "id")
	@JsonIgnore
	@ToString.Exclude
	private Schedule schedule;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "group_id", referencedColumnName = "id")
	@JsonIgnore
	@ToString.Exclude
	private Group group;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subject_id", referencedColumnName = "id")
	@JsonIgnore
	@ToString.Exclude
	private Subject subject;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "teacher_id", referencedColumnName = "id")
	@JsonIgnore
	@ToString.Exclude
	private Teacher teacher;

}
