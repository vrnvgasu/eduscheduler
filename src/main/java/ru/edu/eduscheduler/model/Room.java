package ru.edu.eduscheduler.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import java.util.Set;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "rooms")
@SequenceGenerator(name = "default_gen", sequenceName = "rooms_id_seq", allocationSize = 1)
public class Room extends GenericModel {

	@Column(name = "number", nullable = false, length = 50)
	private String number;

	@OneToMany(mappedBy = "room", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	@ToString.Exclude
	private Set<EducationPlan> educationPlanSet;

}
