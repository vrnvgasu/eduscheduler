package ru.edu.eduscheduler.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import java.util.Objects;
import java.util.Set;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "schedules")
@SequenceGenerator(name = "default_gen", sequenceName = "schedules_id_seq", allocationSize = 1)
public class Schedule extends GenericModel {

	@Column(name = "interval", nullable = false, length = 50)
	private String interval;

	@Column(name = "order", nullable = false)
	private Integer order;

	@OneToMany(mappedBy = "schedule", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
			CascadeType.REFRESH}, fetch = FetchType.LAZY)
	@JsonIgnore
	@ToString.Exclude
	private Set<EducationPlan> educationPlanSet;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Schedule schedule = (Schedule) o;
		return interval.equals(schedule.interval);
	}

	@Override
	public int hashCode() {
		return Objects.hash(interval);
	}

}
