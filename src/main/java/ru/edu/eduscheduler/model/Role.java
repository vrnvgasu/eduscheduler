package ru.edu.eduscheduler.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "roles")
@SequenceGenerator(name = "default_gen", sequenceName = "roles_id_seq", allocationSize = 1)
public class Role extends GenericModel {

	@Column(name = "title", nullable = false, length = 250)
	private String title;

}
