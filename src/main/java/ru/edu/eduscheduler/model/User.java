package ru.edu.eduscheduler.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.edu.eduscheduler.dict.RoleEnum;

@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
@ToString
@Entity
@Table(name = "users")
@SequenceGenerator(name = "default_gen", sequenceName = "users_id_seq", allocationSize = 1)
public class User extends GenericModel {

	@Column(name = "last_name", nullable = false, length = 250)
	private String lastName;

	@Column(name = "first_name", nullable = false, length = 250)
	private String firstName;

	@Column(name = "second_name", length = 250)
	private String secondName;

	@Column(name = "email", nullable = false, length = 250)
	private String email;

	@Column(name = "password", length = 250)
	private String password;

	@Column(name = "role", nullable = false)
	@Enumerated(value = EnumType.STRING)
	private RoleEnum role;

}
