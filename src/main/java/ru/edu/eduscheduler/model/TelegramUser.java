package ru.edu.eduscheduler.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
@ToString
@Entity
@Table(name = "telegram_users")
@SequenceGenerator(name = "default_gen", sequenceName = "telegram_users_id_seq", allocationSize = 1)
public class TelegramUser extends GenericModel {

	@Column(name = "chat_id", nullable = false, unique = true)
	private Long chatId;

	@Column(name = "student_id", updatable = false, insertable = false)
	private Long studentId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "student_id", referencedColumnName = "id")
	@JsonIgnore
	@ToString.Exclude
	private Student student;

}
