package ru.edu.eduscheduler.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "groups")
@SequenceGenerator(name = "default_gen", sequenceName = "groups_id_seq", allocationSize = 1)
public class Group extends GenericModel {

	@Column(name = "number", nullable = false, length = 50)
	private String number;

	@OneToMany(mappedBy = "group", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
			CascadeType.REFRESH}, fetch = FetchType.LAZY)
	@JsonIgnore
	@ToString.Exclude
	private Set<Student> studentSet;

	@OneToMany(mappedBy = "group", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	@ToString.Exclude
	private Set<EducationPlan> educationPlanSet;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH},
			fetch = FetchType.LAZY)
	@JoinTable(name = "group_subject",
			joinColumns = @JoinColumn(name = "group_id"),
			inverseJoinColumns = @JoinColumn(name = "subject_id"))
	@JsonIgnore
	@ToString.Exclude
	private Set<Subject> subjects = new HashSet<>();

}
