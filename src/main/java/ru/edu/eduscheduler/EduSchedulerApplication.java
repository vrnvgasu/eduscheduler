package ru.edu.eduscheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EduSchedulerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EduSchedulerApplication.class, args);
	}

}
