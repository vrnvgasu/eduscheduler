package ru.edu.eduscheduler.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import ru.edu.eduscheduler.services.userdetails.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

	private final CustomUserDetailsService userService;

	public WebSecurityConfig(CustomUserDetailsService userService) {
		this.userService = userService;
	}

	//Создаем бин нашего энкриптора паролей
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public HttpFirewall allowPercent() {
		StrictHttpFirewall firewall = new StrictHttpFirewall();
		firewall.setAllowUrlEncodedPercent(true);
		firewall.setAllowUrlEncodedSlash(true);
		return firewall;
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http
				.csrf()
				.disable()
				.authorizeHttpRequests((requests) -> requests
						.requestMatchers("/login", "/registration", "/remember-password", "/change-password/**")
						.permitAll()
				)
				.authorizeHttpRequests((requests) -> requests
						.requestMatchers(
								"/resources/**",
								"/static/**",
								"/assets/**",
								"/css/**",
								"/js/**",
								"/image/**",
								"/webjars/**"
						)
						.permitAll()
				)
				.formLogin((form) -> form
						.loginPage("/login")
						.permitAll()
						.defaultSuccessUrl("/")
						.permitAll()
				)
				.logout((logout) -> logout
						.permitAll()
						.logoutSuccessUrl("/login")
				)
				.authorizeHttpRequests((requests) -> requests
						.anyRequest()
						.authenticated()
				);
		return http.build();
	}

	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder());
	}

}
