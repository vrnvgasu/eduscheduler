package ru.edu.eduscheduler.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import ru.edu.eduscheduler.services.TelegramBot;

@Component
public class BotInitializer {

	@Value("${bot.start}")
	private Boolean botStart;

	private final TelegramBot bot;

	@Autowired
	public BotInitializer(TelegramBot bot) {
		this.bot = bot;
	}

	@EventListener({ContextRefreshedEvent.class})
	public void init() throws TelegramApiException {
		if (!botStart) {
			return;
		}

		TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
		telegramBotsApi.registerBot(bot);
	}

}
