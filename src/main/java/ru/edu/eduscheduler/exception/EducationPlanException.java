package ru.edu.eduscheduler.exception;

public class EducationPlanException extends RuntimeException {

	public EducationPlanException(String message) {
		super(message);
	}

	public EducationPlanException(String message, Throwable cause) {
		super(message, cause);
	}

	public EducationPlanException(Throwable cause) {
		super(cause);
	}

}
