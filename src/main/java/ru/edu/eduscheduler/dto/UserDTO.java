package ru.edu.eduscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.eduscheduler.dict.RoleEnum;
import ru.edu.eduscheduler.model.User;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO extends BaseDTO {

	private String lastName;

	private String firstName;

	private String secondName;

	private String email;

	private RoleEnum role;

	public UserDTO(User user) {
		super(user.getId());
		this.lastName = user.getLastName();
		this.firstName = user.getFirstName();
		this.secondName = user.getSecondName();
		this.email = user.getEmail();
		this.role = user.getRole();
	}

	public String getFullName() {
		return lastName + " " + firstName + " " + secondName;
	}

}
