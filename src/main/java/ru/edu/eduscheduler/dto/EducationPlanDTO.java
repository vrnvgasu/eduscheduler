package ru.edu.eduscheduler.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.eduscheduler.model.EducationPlan;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EducationPlanDTO extends BaseDTO {

	@NotNull
	@JsonFormat(pattern = "dd-MM-yyyy")
	private LocalDate date;

	@Min(value = 1)
	@NotNull
	private Long roomId;

	@Min(value = 1)
	@NotNull
	private Long scheduleId;

	@Min(value = 1)
	@NotNull
	private Long groupId;

	@Min(value = 1)
	@NotNull
	private Long subjectId;

	@Min(value = 1)
	private Long teacherId;

	private RoomDTO roomDTO;

	private ScheduleDTO scheduleDTO;

	private GroupDTO groupDTO;

	private SubjectDTO subjectDTO;

	private TeacherDTO teacherDTO;

	public EducationPlanDTO(EducationPlan educationPlan) {
		date = educationPlan.getDate();
		roomId = educationPlan.getRoomId();
		scheduleId = educationPlan.getScheduleId();
		groupId = educationPlan.getGroupId();
		subjectId = educationPlan.getSubjectId();
		teacherId = educationPlan.getTeacherId();
	}

}
