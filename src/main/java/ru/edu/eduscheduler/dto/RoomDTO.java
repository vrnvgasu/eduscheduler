package ru.edu.eduscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.eduscheduler.model.Room;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RoomDTO extends BaseDTO {

	private String number;

	public RoomDTO(Room room) {
		super(room.getId());
		this.number = room.getNumber();
	}

}
