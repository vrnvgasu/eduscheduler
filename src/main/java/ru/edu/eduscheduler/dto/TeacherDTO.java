package ru.edu.eduscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.eduscheduler.model.Teacher;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TeacherDTO extends BaseDTO {

	private String position;

	private Long userId;

	private UserDTO userDTO;

	public TeacherDTO(Teacher teacher) {
		super(teacher.getId());
		this.position = teacher.getPosition();
		this.userId = teacher.getUserId();
	}

}
