package ru.edu.eduscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO extends BaseDTO {

	private String idNumber;

	private Long groupId;

	private Long userId;

	private GroupDTO groupDTO;

	private UserDTO userDTO;

}
