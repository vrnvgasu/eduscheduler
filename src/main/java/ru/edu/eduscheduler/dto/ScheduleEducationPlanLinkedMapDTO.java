package ru.edu.eduscheduler.dto;

import java.util.LinkedHashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.eduscheduler.model.Schedule;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleEducationPlanLinkedMapDTO {

	private Map<Schedule, EducationPlanForViewDTO> linkedMap = new LinkedHashMap<>();

}
