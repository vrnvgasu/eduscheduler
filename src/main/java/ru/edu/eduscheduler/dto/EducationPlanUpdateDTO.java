package ru.edu.eduscheduler.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EducationPlanUpdateDTO {

	@Min(value = 1)
	@NotNull
	private Long roomId;

	@Min(value = 1)
	@NotNull
	private Long subjectId;

	@Min(value = 1)
	private Long teacherId;

}
