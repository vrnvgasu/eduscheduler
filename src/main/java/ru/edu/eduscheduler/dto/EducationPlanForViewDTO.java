package ru.edu.eduscheduler.dto;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.eduscheduler.model.EducationPlan;
import ru.edu.eduscheduler.model.Group;
import ru.edu.eduscheduler.model.Room;
import ru.edu.eduscheduler.model.Schedule;
import ru.edu.eduscheduler.model.Subject;
import ru.edu.eduscheduler.model.Teacher;
import ru.edu.eduscheduler.model.User;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EducationPlanForViewDTO extends BaseDTO {

	private LocalDate date;

	private Long roomId;

	private String roomNumber;

	private Long scheduleId;

	private String scheduleInterval;

	private Integer scheduleOrder;

	private Long groupId;

	private String groupNumber;

	private Long subjectId;

	private String subjecTitle;

	private Long teacherId;

	private String teacherPosition;

	private Long teacherUserId;

	private String teacherFIO;

	public EducationPlanForViewDTO(EducationPlan educationPlan, Room room, Schedule schedule, Group group,
			Subject subject, Teacher teacher, User user) {
		super(educationPlan.getId());
		date = educationPlan.getDate();

		roomId = room.getId();
		roomNumber = room.getNumber();

		scheduleId = educationPlan.getScheduleId();
		scheduleInterval = schedule.getInterval();
		scheduleOrder = schedule.getOrder();

		groupId = group.getId();
		groupNumber = group.getNumber();

		subjectId = subject.getId();
		subjecTitle = subject.getTitle();

		teacherId = teacher.getId();
		teacherPosition = teacher.getPosition();
		teacherUserId = teacher.getUserId();
		teacherFIO = user.getLastName() + " " + user.getFirstName();
	}

}
