package ru.edu.eduscheduler.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserFormDTO {

	@NotEmpty
	private String lastName;

	@NotEmpty
	private String firstName;

	private String secondName;

	@NotEmpty
	private String email;

	@NotEmpty
	private String password;

}
