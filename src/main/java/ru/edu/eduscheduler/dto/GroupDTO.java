package ru.edu.eduscheduler.dto;

import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.eduscheduler.model.Group;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GroupDTO extends BaseDTO {

	private String number;

	private Set<SubjectDTO> subjectDTOSet = new HashSet<>();

	public GroupDTO(Group group) {
		super(group.getId());
		this.number = group.getNumber();
	}

}
