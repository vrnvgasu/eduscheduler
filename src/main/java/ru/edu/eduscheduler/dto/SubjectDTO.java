package ru.edu.eduscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.eduscheduler.model.Subject;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubjectDTO extends BaseDTO {

	private String title;

	public SubjectDTO(Subject subject) {
		super(subject.getId());
		this.title = subject.getTitle();
	}

}
