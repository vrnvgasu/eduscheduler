package ru.edu.eduscheduler.dto;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DateEducationPlanLinkedMapDTO {

	private Map<LocalDate, ScheduleEducationPlanLinkedMapDTO> linkedMapDTO = new LinkedHashMap<>();

}
