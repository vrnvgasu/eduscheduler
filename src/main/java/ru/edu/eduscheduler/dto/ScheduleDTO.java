package ru.edu.eduscheduler.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.eduscheduler.model.Schedule;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleDTO extends BaseDTO {

	private String interval;

	private Integer order;

	public ScheduleDTO(Schedule schedule) {
		super(schedule.getId());
		this.interval = schedule.getInterval();
		this.order = schedule.getOrder();
	}

}
