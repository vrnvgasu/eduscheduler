package ru.edu.eduscheduler.MVC.controller;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.edu.eduscheduler.dto.ChangePasswordFormDTO;
import ru.edu.eduscheduler.dto.EmailFormDTO;
import ru.edu.eduscheduler.dto.UserFormDTO;
import ru.edu.eduscheduler.model.User;
import ru.edu.eduscheduler.services.UserService;

@Slf4j
@Controller
public class AuthController {

	private final UserService userService;

	@Autowired
	public AuthController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/login")
	public String login() {
		if (
				SecurityContextHolder.getContext().getAuthentication() != null &&
						SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
						!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)
		) {
			return "redirect:/";
		}
		return "auth/login";
	}

	@GetMapping("/registration")
	public String registrationPage() {
		if (
				SecurityContextHolder.getContext().getAuthentication() != null &&
						SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
						!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)
		) {
			return "redirect:/";
		}

		return "auth/registration";
	}

	@PostMapping("/registration")
	public String createUser(@ModelAttribute("userForm") @Valid UserFormDTO userFormDTO) {
		log.info("CОЗДАНИЕ НОВОГО ПОЛЬЗОВАТЕЛЯ: " + userFormDTO.toString());
		userService.createFromDTO(userFormDTO);
		return "redirect:/login";
	}

	@GetMapping("/remember-password")
	public String changePassword() {
		return "auth/rememberPassword";
	}

	@PostMapping("/remember-password")
	public String changePassword(@ModelAttribute("emailForm") @Valid EmailFormDTO formDTO) {
		log.info("!!!Changing password!!!!");
		User user = userService.getUserByEmail(formDTO.getEmail());
		userService.sendChangePasswordEmail(formDTO.getEmail(), user.getId());
		return "redirect:/login";
	}

	@GetMapping("/change-password/{userId}")
	public String changePasswordAfterEmailSent(@PathVariable Long userId, Model model) {
		model.addAttribute("userId", userId);
		return "auth/changePassword";
	}

	@PostMapping("/change-password/{userId}")
	public String changePassword(@PathVariable Long userId,
			@ModelAttribute("changePasswordForm") @Valid ChangePasswordFormDTO formDTO) {
		userService.changePassword(userId, formDTO.getPassword());
		return "redirect:/login";
	}

}
