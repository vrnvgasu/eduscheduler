package ru.edu.eduscheduler.MVC.controller;

import jakarta.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.eduscheduler.dto.EducationPlanDTO;
import ru.edu.eduscheduler.dto.EducationPlanUpdateDTO;
import ru.edu.eduscheduler.model.EducationPlan;
import ru.edu.eduscheduler.services.EducationPlanService;
import ru.edu.eduscheduler.services.GroupService;
import ru.edu.eduscheduler.services.RoomService;
import ru.edu.eduscheduler.services.ScheduleService;
import ru.edu.eduscheduler.services.SubjectService;
import ru.edu.eduscheduler.services.TeacherService;
import ru.edu.eduscheduler.util.DateParser;

@Controller
@RequestMapping("/education_plans")
public class EducationPlanController {

	private final EducationPlanService educationPlanService;

	private final GroupService groupService;

	private final ScheduleService scheduleService;

	private final RoomService roomService;

	private final TeacherService teacherService;

	private final SubjectService subjectService;

	@Autowired
	public EducationPlanController(EducationPlanService educationPlanService, GroupService groupService,
			ScheduleService scheduleService, RoomService roomService, TeacherService teacherService,
			SubjectService subjectService) {
		this.educationPlanService = educationPlanService;
		this.groupService = groupService;
		this.scheduleService = scheduleService;
		this.roomService = roomService;
		this.teacherService = teacherService;
		this.subjectService = subjectService;
	}

	@GetMapping("/room/{roomId}")
	public String getForRoom(@PathVariable Long roomId, @RequestParam(value = "date", required = false) String date,
			Model model) {
//		LocalDate localDate = date == null ? DateParser.parse() : LocalDate.parse(date);
		LocalDate localDate = date == null ? DateParser.parse("04-09-2023") : LocalDate.parse(date);
		model.addAttribute("dateEducationPlanLinkedMap", educationPlanService.getLinkedMapForRoom(
				roomId,
				localDate
		));
		model.addAttribute("room", roomService.findById(roomId));
		model.addAttribute("schedules", scheduleService.getAllOrdered());
		model.addAttribute("date", localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

		return "views/educationPlan/room";
	}

	@GetMapping("/teacher/{teacherId}")
	public String getForTeacher(@PathVariable Long teacherId, @RequestParam(value = "date", required = false) String date,
			Model model) {
//		LocalDate localDate = date == null ? DateParser.parse() : LocalDate.parse(date);
		LocalDate localDate = date == null ? DateParser.parse("04-09-2023") : LocalDate.parse(date);
		model.addAttribute("dateEducationPlanLinkedMap", educationPlanService.getLinkedMapForTeacher(
				teacherId,
				localDate
		));
		model.addAttribute("teacher", teacherService.findById(teacherId));
		model.addAttribute("schedules", scheduleService.getAllOrdered());
		model.addAttribute("date", localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

		return "views/educationPlan/teacher";
	}

	@GetMapping("/group/{groupId}")
	public String getForGroup(@PathVariable Long groupId, @RequestParam(value = "date", required = false) String date,
			Model model) {
//		LocalDate localDate = date == null ? DateParser.parse() : LocalDate.parse(date);
		LocalDate localDate = date == null ? DateParser.parse("04-09-2023") : LocalDate.parse(date);
		model.addAttribute("dateEducationPlanLinkedMap", educationPlanService.getLinkedMapForGroup(
				groupId,
				localDate
		));
		model.addAttribute("groupDTO", groupService.getDTOById(groupId));
		model.addAttribute("schedules", scheduleService.getAllOrdered());
		model.addAttribute("date", localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

		return "views/educationPlan/group";
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
	@GetMapping("/create")
	public String create(@RequestParam(value = "date") String date, @RequestParam(value = "scheduleId") Long scheduleId,
			@RequestParam(value = "groupId") Long groupId,
			@ModelAttribute("educationPlan") EducationPlanDTO educationPlanDTO, Model model) {
		model.addAttribute("date", date);
		model.addAttribute("scheduleDTO", scheduleService.getDTOById(scheduleId));
		model.addAttribute("groupDTO", groupService.getDTOById(groupId));
		model.addAttribute("subjectDTOList", subjectService.getListAvailableForGroup(groupId));
		model.addAttribute("roomList", roomService.getListAvailable(LocalDate.parse(date), scheduleId));
		model.addAttribute("teacherList", teacherService.getListAvailable(LocalDate.parse(date), scheduleId));

		return "views/educationPlan/add";
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
	@PostMapping("/add")
	public String add(@ModelAttribute("educationPlan") @Valid EducationPlanDTO educationPlanDTO) {
		educationPlanService.saveFromDTO(educationPlanDTO);

		return "redirect:/education_plans/group/" + educationPlanDTO.getGroupId()
				+ "?date=" + educationPlanDTO.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
	@GetMapping("/edit/{educationPlanId}")
	public String edit(@PathVariable Long educationPlanId, Model model) {
		EducationPlan educationPlan = educationPlanService.findByIdWithScheduleAndGroup(educationPlanId);
		model.addAttribute("educationPlan", educationPlan);
		model.addAttribute("date", educationPlan.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		model.addAttribute("schedule", educationPlan.getSchedule());
		model.addAttribute("group", educationPlan.getGroup());
		model.addAttribute("subjectDTOList", subjectService.getListAvailableForGroup(educationPlan.getGroup().getId()));
		model.addAttribute("roomList", roomService.getListAvailableForEdit(educationPlan.getDate(), educationPlan));
		model.addAttribute("teacherList", teacherService.getListAvailableForEdit(educationPlan.getDate(), educationPlan));

		return "views/educationPlan/edit";
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
	@PostMapping("/update/{educationPlanId}")
	public String update(@PathVariable Long educationPlanId,
			@ModelAttribute("educationPlan") @Valid EducationPlanUpdateDTO educationPlanDTO) {
		EducationPlan educationPlan = educationPlanService.updateFromDTO(educationPlanId, educationPlanDTO);

		return "redirect:/education_plans/group/" + educationPlan.getGroupId()
				+ "?date=" + educationPlan.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
	@PostMapping("/delete/{educationPlanId}")
	public String delete(@PathVariable Long educationPlanId) {
		EducationPlan educationPlan = educationPlanService.findById(educationPlanId);
		educationPlanService.deleteById(educationPlanId);

		return "redirect:/education_plans/group/" + educationPlan.getGroupId()
				+ "?date=" + educationPlan.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

}
