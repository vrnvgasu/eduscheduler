package ru.edu.eduscheduler.MVC.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.eduscheduler.dto.TeacherDTO;
import ru.edu.eduscheduler.services.TeacherService;

@Controller
@RequestMapping("/teachers")
public class TeachersController {

	private final TeacherService teacherService;

	@Autowired
	public TeachersController(TeacherService teacherService) {
		this.teacherService = teacherService;
	}

	@GetMapping
	public String index(Model model,
			@RequestParam(value = "page", defaultValue = "1") Optional<Integer> page) {
		Page<TeacherDTO> pagination = teacherService.getDTOListPagination(page);
		model.addAttribute("pagination", pagination);

		int totalPages = pagination.getTotalPages();
		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
					.boxed()
					.collect(Collectors.toList());
			model.addAttribute("pageNumbers", pageNumbers);
		}

		return "views/teacher/list";
	}

}
