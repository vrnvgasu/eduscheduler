package ru.edu.eduscheduler.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateParser {

	public static LocalDate parse(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		return LocalDate.parse(date, formatter);
	}

}
