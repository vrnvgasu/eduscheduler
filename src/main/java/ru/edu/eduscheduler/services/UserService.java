package ru.edu.eduscheduler.services;

import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.edu.eduscheduler.dict.RoleEnum;
import ru.edu.eduscheduler.dto.UserFormDTO;
import ru.edu.eduscheduler.exception.NotFoundException;
import ru.edu.eduscheduler.model.User;
import ru.edu.eduscheduler.repository.UserRepository;

@Service
public class UserService {

	@Value("${email.change-password-url}")
	private String changePasswordUrl = "http://localhost:9100/users/change-password/";

	private final UserRepository repository;

	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	private final JavaMailSender javaMailSender;

	public UserService(UserRepository repository, BCryptPasswordEncoder bCryptPasswordEncoder,
			JavaMailSender javaMailSender) {
		this.repository = repository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.javaMailSender = javaMailSender;
	}

	public void createFromDTO(UserFormDTO userFormDTO) {
		User user = User.builder()
				.lastName(userFormDTO.getLastName())
				.firstName(userFormDTO.getFirstName())
				.secondName(userFormDTO.getSecondName())
				.email(userFormDTO.getEmail())
				.password(bCryptPasswordEncoder.encode(userFormDTO.getPassword()))
				.role(RoleEnum.ROLE_GUEST)
				.build();
		user.setCreatedAt(LocalDateTime.now());
		user.setUpdatedAt(LocalDateTime.now());
		repository.save(user);
	}

	public User getUserByEmail(String email) {
		return repository.findUserByEmail(email);
	}

	public void sendChangePasswordEmail(final String email, final Long userId) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(email);
		message.setSubject("Восстановление пароля на сервисе \"Система расписания\"");
		message.setText(
				"Добрый день! Вы получили это письмо, так как с вашего аккаунта была отправлена заявка на восстановление пароля. "
						+
						"Для восстановления пароля, перейдите по ссылке: '" + this.changePasswordUrl + userId + "'");
		javaMailSender.send(message);

	}

	public void changePassword(Long userId, String password) {
		User user = repository.findById(userId)
				.orElseThrow(() -> new NotFoundException("User with usch ID: " + userId + " not found."));
		user.setPassword(bCryptPasswordEncoder.encode(password));
		user.setUpdatedAt(LocalDateTime.now());
		repository.save(user);
	}

}
