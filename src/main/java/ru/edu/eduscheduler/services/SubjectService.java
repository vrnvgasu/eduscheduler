package ru.edu.eduscheduler.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.stereotype.Service;
import ru.edu.eduscheduler.dto.SubjectDTO;
import ru.edu.eduscheduler.exception.NotFoundException;
import ru.edu.eduscheduler.model.Subject;
import ru.edu.eduscheduler.repository.SubjectRepository;

@Service
public class SubjectService {

	private final SubjectRepository repository;

	public SubjectService(SubjectRepository repository) {
		this.repository = repository;
	}

	public Subject findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Subject with ID=" + id + " not found"));
	}

	public List<SubjectDTO> getDTOList() {
		List<SubjectDTO> subjectDTOList = new ArrayList<>();

		for (Subject subject: repository.findAll()) {
			subjectDTOList.add(new SubjectDTO(subject));
		}

		return subjectDTOList;
	}

	public Subject save(Subject subject) {
		return repository.save(subject);
	}

	public Set<Subject> getListAvailableForGroup(Long groupId) {
		return repository.getListAvailableForGroup(groupId);
	}

}
