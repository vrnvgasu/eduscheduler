package ru.edu.eduscheduler.services;

import java.util.Set;
import org.springframework.stereotype.Service;
import ru.edu.eduscheduler.dto.ScheduleDTO;
import ru.edu.eduscheduler.exception.NotFoundException;
import ru.edu.eduscheduler.model.Schedule;
import ru.edu.eduscheduler.repository.ScheduleRepository;

@Service
public class ScheduleService {

	private final ScheduleRepository repository;

	public ScheduleService(ScheduleRepository repository) {
		this.repository = repository;
	}
	
	public Set<Schedule> getAllOrdered() {
		return repository.findAllByOrderByOrder();
	}

	public ScheduleDTO getDTOById(Long scheduleId) {
		return new ScheduleDTO(findById(scheduleId));
	}

	public Schedule findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Schedule with ID=" + id + " not found"));
	}

}
