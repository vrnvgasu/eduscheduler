package ru.edu.eduscheduler.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.edu.eduscheduler.dto.RoomDTO;
import ru.edu.eduscheduler.exception.NotFoundException;
import ru.edu.eduscheduler.model.EducationPlan;
import ru.edu.eduscheduler.model.Room;
import ru.edu.eduscheduler.repository.RoomRepository;

@Service
public class RoomService {

	private static final int PAGE_SIZE = 5;

	private final RoomRepository repository;

	public RoomService(RoomRepository repository) {
		this.repository = repository;
	}

	public Room findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Room with ID=" + id + " not found"));
	}

	public List<RoomDTO> getDTOList() {
		List<RoomDTO> roomDTOList = new ArrayList<>();

		for (Room room: repository.findAll()) {
			RoomDTO roomDTO = new RoomDTO(room);
			roomDTOList.add(roomDTO);
		}

		return roomDTOList;
	}

	public List<Room> getListAvailable(LocalDate localDate, Long scheduleId) {
		return repository.getListAvailable(localDate, scheduleId);
	}

	public List<Room> getListAvailableForEdit(LocalDate localDate, EducationPlan educationPlan) {
		List<Room> roomSet = this.getListAvailable(localDate, educationPlan.getScheduleId());
		roomSet.add(findById(educationPlan.getRoomId()));
		return roomSet;
	}

	public Page<RoomDTO> getDTOListPagination(Optional<Integer> page) {
		int currentPage = page.orElse(1);
		return findPaginated(PageRequest.of(currentPage - 1, PAGE_SIZE));
	}

	public Page<RoomDTO> findPaginated(Pageable pageable) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		Page<Room> roomList = repository.findAll(pageable);
		List<RoomDTO> roomDTOList = new ArrayList<>();

		for (Room room : roomList) {
			roomDTOList.add(new RoomDTO(room));
		}

		Page<RoomDTO> roomDTOPage = new PageImpl<>(
				roomDTOList,
				PageRequest.of(currentPage, pageSize),
				roomList.getTotalElements()
		);

		return roomDTOPage;
	}

}
