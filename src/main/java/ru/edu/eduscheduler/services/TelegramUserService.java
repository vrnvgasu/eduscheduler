package ru.edu.eduscheduler.services;

import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.stereotype.Service;
import ru.edu.eduscheduler.model.Student;
import ru.edu.eduscheduler.model.TelegramUser;
import ru.edu.eduscheduler.repository.TelegramUserRepository;

@Service
public class TelegramUserService {

	private final TelegramUserRepository repository;

	public TelegramUserService(TelegramUserRepository repository) {
		this.repository = repository;
	}

	public TelegramUser findOrCreate(long chatId) {
		Optional<TelegramUser> optionalTelegramUser = repository.findByChatId(chatId);
		TelegramUser telegramUser;

		if (optionalTelegramUser.isEmpty()) {
			telegramUser = TelegramUser.builder()
					.chatId(chatId)
					.build();
			telegramUser.setCreatedAt(LocalDateTime.now());
			telegramUser.setUpdatedAt(LocalDateTime.now());
			telegramUser = repository.save(telegramUser);
		} else {
			telegramUser = optionalTelegramUser.get();
		}

		return telegramUser;
	}

	public void setStudent(TelegramUser telegramUser, Student student) {
		telegramUser.setStudent(student);
		repository.save(telegramUser);
	}

}
