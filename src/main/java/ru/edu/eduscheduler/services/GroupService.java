package ru.edu.eduscheduler.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.edu.eduscheduler.dto.GroupDTO;
import ru.edu.eduscheduler.exception.NotFoundException;
import ru.edu.eduscheduler.model.Group;
import ru.edu.eduscheduler.repository.GroupRepository;

@Service
public class GroupService {

	private static final int PAGE_SIZE = 5;

	private final GroupRepository repository;

	public GroupService(GroupRepository repository) {
		this.repository = repository;
	}

	public Group findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Group with ID=" + id + " not found"));
	}

	public GroupDTO getDTOById(Long id) {
		return new GroupDTO(findById(id));
	}

	public Page<GroupDTO> getDTOListPagination(Optional<Integer> page) {
		int currentPage = page.orElse(1);
		return findPaginated(PageRequest.of(currentPage - 1, PAGE_SIZE));
	}

	public Page<GroupDTO> findPaginated(Pageable pageable) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		Page<Group> groupList = repository.findAll(pageable);
		List<GroupDTO> groupDTOList = new ArrayList<>();

		for (Group group : groupList) {
			groupDTOList.add(new GroupDTO(group));
		}

		Page<GroupDTO> groupDTOPage = new PageImpl<>(
				groupDTOList,
				PageRequest.of(currentPage, pageSize),
				groupList.getTotalElements()
		);

		return groupDTOPage;
	}

	public List<GroupDTO> getDTOList() {
		List<GroupDTO> groupDTOList = new ArrayList<>();

		for (Group group: repository.findAll()) {
			groupDTOList.add(new GroupDTO(group));
		}

		return groupDTOList;
	}

	public Group save(Group group) {
		return repository.save(group);
	}

}
