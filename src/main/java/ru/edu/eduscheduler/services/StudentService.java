package ru.edu.eduscheduler.services;

import java.util.Optional;
import org.springframework.stereotype.Service;
import ru.edu.eduscheduler.model.Student;
import ru.edu.eduscheduler.repository.StudentRepository;

@Service
public class StudentService {

	private final StudentRepository repository;

	public StudentService(StudentRepository repository) {
		this.repository = repository;
	}

	public Optional<Student> findByIdNumber(Long idNumber) {
		return repository.findByIdNumber(idNumber);
	}

}
