package ru.edu.eduscheduler.services;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.stereotype.Service;
import ru.edu.eduscheduler.dto.DateEducationPlanLinkedMapDTO;
import ru.edu.eduscheduler.dto.EducationPlanDTO;
import ru.edu.eduscheduler.dto.EducationPlanForViewDTO;
import ru.edu.eduscheduler.dto.EducationPlanUpdateDTO;
import ru.edu.eduscheduler.dto.ScheduleEducationPlanLinkedMapDTO;
import ru.edu.eduscheduler.exception.EducationPlanException;
import ru.edu.eduscheduler.exception.NotFoundException;
import ru.edu.eduscheduler.model.EducationPlan;
import ru.edu.eduscheduler.model.Schedule;
import ru.edu.eduscheduler.repository.EducationPlanRepository;

@Service
public class EducationPlanService {

	private static final int DAYS_FOR_VIEW = 7;

	private final EducationPlanRepository repository;

	private final ScheduleService scheduleService;

	private final RoomService roomService;

	private final GroupService groupService;

	private final SubjectService subjectService;

	private final TeacherService teacherService;

	public EducationPlanService(EducationPlanRepository repository, ScheduleService scheduleService,
			RoomService roomService, GroupService groupService, SubjectService subjectService, TeacherService teacherService) {
		this.repository = repository;
		this.scheduleService = scheduleService;
		this.roomService = roomService;
		this.groupService = groupService;
		this.subjectService = subjectService;
		this.teacherService = teacherService;
	}

	public DateEducationPlanLinkedMapDTO getLinkedMapForGroup(Long groupId, LocalDate startDate) {
		Set<EducationPlan> educationPlanSet = repository.getForGroup(
				groupId,
				startDate,
				startDate.plusDays(DAYS_FOR_VIEW - 1)
		);

		return getEducationPlanLinkedMap(educationPlanSet, startDate);
	}

	public DateEducationPlanLinkedMapDTO getLinkedMapForRoom(Long roomId, LocalDate startDate) {
		Set<EducationPlan> educationPlanSet = repository.getForRoom(
				roomId,
				startDate,
				startDate.plusDays(DAYS_FOR_VIEW - 1)
		);

		return getEducationPlanLinkedMap(educationPlanSet, startDate);
	}

	public DateEducationPlanLinkedMapDTO getLinkedMapForTeacher(Long teacherId, LocalDate startDate) {
		Set<EducationPlan> educationPlanSet = repository.getForTeacher(
				teacherId,
				startDate,
				startDate.plusDays(DAYS_FOR_VIEW - 1)
		);

		return getEducationPlanLinkedMap(educationPlanSet, startDate);
	}

	public DateEducationPlanLinkedMapDTO getEducationPlanLinkedMap(Set<EducationPlan> educationPlanSet,
			LocalDate StartDate) {
		Set<Schedule> scheduleSet = scheduleService.getAllOrdered();
		DateEducationPlanLinkedMapDTO dateEducationPlanMapDTO = new DateEducationPlanLinkedMapDTO();

		for (int i = 0; i < DAYS_FOR_VIEW; i++) {
			LocalDate dateTemp = StartDate.plusDays(i);
			ScheduleEducationPlanLinkedMapDTO scheduleEducationPlanMapDTO = new ScheduleEducationPlanLinkedMapDTO();

			for (Schedule schedule : scheduleSet) {
				EducationPlanForViewDTO educationPlanDTO = new EducationPlanForViewDTO();

				for (EducationPlan educationPlan : educationPlanSet) {
					if (!educationPlan.getDate().equals(dateTemp)) {
						continue;
					}
					if (!educationPlan.getSchedule().getOrder().equals(schedule.getOrder())) {
						continue;
					}

					educationPlanDTO = new EducationPlanForViewDTO(
							educationPlan,
							educationPlan.getRoom(),
							educationPlan.getSchedule(),
							educationPlan.getGroup(),
							educationPlan.getSubject(),
							educationPlan.getTeacher(),
							educationPlan.getTeacher().getUser()
					);
				}

				scheduleEducationPlanMapDTO.getLinkedMap().put(schedule, educationPlanDTO);
			}

			dateEducationPlanMapDTO.getLinkedMapDTO().put(dateTemp, scheduleEducationPlanMapDTO);
		}

		return dateEducationPlanMapDTO;
	}

	public EducationPlan findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Education Plan with ID=" + id + " not found"));
	}

	public EducationPlan findByIdWithScheduleAndGroup(Long id) {
		return repository.findByIdWithScheduleAndGroup(id)
				.orElseThrow(() -> new NotFoundException("Education Plan with ID=" + id + " not found"));
	}

	public EducationPlan saveFromDTO(EducationPlanDTO educationPlanDTO) {
		if (repository.existsByDateAndRoomIdAndScheduleId(educationPlanDTO.getDate(), educationPlanDTO.getRoomId(), educationPlanDTO.getScheduleId())) {
			throw new EducationPlanException("Try to create education plan on date, room and time that exists");
		}
		if (repository.existsByDateAndGroupIdAndScheduleId(educationPlanDTO.getDate(), educationPlanDTO.getGroupId(), educationPlanDTO.getScheduleId())) {
			throw new EducationPlanException("Try to create education plan for date, group and time that exists");
		}

		EducationPlan educationPlan = EducationPlan.builder()
				.date(educationPlanDTO.getDate())
				.room(roomService.findById(educationPlanDTO.getRoomId()))
				.schedule(scheduleService.findById(educationPlanDTO.getScheduleId()))
				.group(groupService.findById(educationPlanDTO.getGroupId()))
				.subject(subjectService.findById(educationPlanDTO.getSubjectId()))
				.teacher(teacherService.findById(educationPlanDTO.getTeacherId()))
				.build();
		educationPlan.setCreatedAt(LocalDateTime.now());
		educationPlan.setUpdatedAt(LocalDateTime.now());

		return repository.save(educationPlan);
	}

	public LinkedHashSet<EducationPlan> getScheduleForStudent(Long studentId, LocalDate date) {
		return repository.getScheduleForStudent(studentId, date);
	}

	public LinkedHashSet<LocalDate> getNearestEducationDaysForStudent(Long studentId) {
		LinkedHashSet<LocalDate> dates = new LinkedHashSet<>();

		for (Date date: repository.getNearestEducationDaysForStudent(studentId, LocalDate.now())) {
			dates.add(date.toLocalDate());
		}

		return dates;
	}

	public EducationPlan updateFromDTO(Long educationPlanId, EducationPlanUpdateDTO educationPlanDTO) {
		EducationPlan educationPlan = findById(educationPlanId);
		educationPlan.setRoom(roomService.findById(educationPlanDTO.getRoomId()));
		educationPlan.setSubject(subjectService.findById(educationPlanDTO.getSubjectId()));
		educationPlan.setTeacher(teacherService.findById(educationPlanDTO.getTeacherId()));
		educationPlan.setUpdatedAt(LocalDateTime.now());

		return repository.save(educationPlan);
	}

	public void deleteById(Long educationPlanId) {
		repository.deleteById(educationPlanId);
	}

}
