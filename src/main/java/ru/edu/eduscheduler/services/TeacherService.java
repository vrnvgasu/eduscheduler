package ru.edu.eduscheduler.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.edu.eduscheduler.dto.TeacherDTO;
import ru.edu.eduscheduler.dto.UserDTO;
import ru.edu.eduscheduler.exception.NotFoundException;
import ru.edu.eduscheduler.model.EducationPlan;
import ru.edu.eduscheduler.model.Teacher;
import ru.edu.eduscheduler.repository.TeacherRepository;

@Service
public class TeacherService {

	private static final int PAGE_SIZE = 5;

	private final TeacherRepository repository;

	public TeacherService(TeacherRepository repository) {
		this.repository = repository;
	}

	public Teacher findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Teacher with ID=" + id + " not found"));
	}

	public List<Teacher> getListAvailable(LocalDate localDate, Long scheduleId) {
		return repository.getListWithUser(localDate, scheduleId);
	}

	public List<Teacher> getListAvailableForEdit(LocalDate localDate, EducationPlan educationPlan) {
		List<Teacher> teacherList = this.getListAvailable(localDate, educationPlan.getScheduleId());
		teacherList.add(findById(educationPlan.getTeacherId()));
		return teacherList;
	}

	public Page<TeacherDTO> getDTOListPagination(Optional<Integer> page) {
		int currentPage = page.orElse(1);
		return findPaginated(PageRequest.of(currentPage - 1, PAGE_SIZE));
	}

	public Page<TeacherDTO> findPaginated(Pageable pageable) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		Page<Teacher> teacherList = repository.findAll(pageable);
		List<TeacherDTO> teacherDTOList = new ArrayList<>();

		for (Teacher teacher : teacherList) {
			TeacherDTO teacherDTO = new TeacherDTO(teacher);
			teacherDTO.setUserDTO(new UserDTO(teacher.getUser()));
			teacherDTOList.add(teacherDTO);
		}

		return new PageImpl<>(
				teacherDTOList,
				PageRequest.of(currentPage, pageSize),
				teacherList.getTotalElements()
		);
	}

}
