package ru.edu.eduscheduler.services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.edu.eduscheduler.config.BotConfig;
import ru.edu.eduscheduler.model.EducationPlan;
import ru.edu.eduscheduler.model.Student;
import ru.edu.eduscheduler.model.TelegramUser;
import ru.edu.eduscheduler.util.DateParser;

@Component
public class TelegramBot extends TelegramLongPollingBot {

	private static final String STUDENT_NOT_FOUND_MESSAGE = "Пользователь не найден:( Попробуйте еще раз. \nУкажите номер студенческого билета";

	private static final String GREETING_MESSAGE = "Добро пожаловать";

	private static final String GET_SCHEDULE_MESSAGE = "Укажите дату в формате дд-mm-yyyy \n(пример: 01-09-2023)";

	private static final String FORMAT_ERROR_MESSAGE = "Неверный формат";

	private static final String AUTH_MESSAGE = "Укажите номер студенческого билета";

	private static final String NO_EDUCATION_MESSAGE = "Занятий не запланировано";

	private static final String NEAREST_DATES_BUTTON = "Ближайшие учебные даты";

	private final BotConfig config;

	private final TelegramUserService telegramUserService;

	private final StudentService studentService;

	private final EducationPlanService educationPlanService;

	@Autowired
	public TelegramBot(BotConfig config, TelegramUserService telegramUserService, StudentService studentService,
			EducationPlanService educationPlanService) {
		this.config = config;
		this.telegramUserService = telegramUserService;
		this.studentService = studentService;
		this.educationPlanService = educationPlanService;
	}

	@Override
	public String getBotUsername() {
		return config.getBotName();
	}

	@Override
	public String getBotToken() {
		return config.getToken();
	}

	@Override
	public void onUpdateReceived(Update update) {

		if (!update.hasMessage() || !update.getMessage().hasText()) {
			return;
		}

		String messageText = update.getMessage().getText();
		long chatId = update.getMessage().getChatId();

		TelegramUser telegramUser = telegramUserService.findOrCreate(chatId);

		if (telegramUser.getStudentId() == null) {
			startCommandReceived(telegramUser, messageText);
			return;
		}

		if (messageText.equals(NEAREST_DATES_BUTTON)) {
			sendMessage(telegramUser.getChatId(), prepareMessageWithEducationDays(telegramUser.getStudentId()), true);
			sendMessage(telegramUser.getChatId(), GET_SCHEDULE_MESSAGE, true);
			return;
		}

		LocalDate date;
		try {
			date = DateParser.parse(messageText);
		} catch (DateTimeParseException e) {
			sendMessage(telegramUser.getChatId(), FORMAT_ERROR_MESSAGE, false);
			sendMessage(telegramUser.getChatId(), GET_SCHEDULE_MESSAGE, true);
			return;
		}

		LinkedHashSet<EducationPlan> educationPlanSet = educationPlanService.getScheduleForStudent(
				telegramUser.getStudentId(),
				date
		);

		sendMessage(telegramUser.getChatId(), prepareMessageWithEducationPlan(educationPlanSet, date), true);
		sendMessage(telegramUser.getChatId(), GET_SCHEDULE_MESSAGE, true);
	}

	private String prepareMessageWithEducationDays(Long studentId) {
		LinkedHashSet<LocalDate> days = educationPlanService.getNearestEducationDaysForStudent(studentId);

		if (days.isEmpty()) {
			return NO_EDUCATION_MESSAGE;
		}

		StringBuilder stringBuilder = new StringBuilder("Ближайшие учебные дни: \n");
		for (LocalDate localDate : days) {
			stringBuilder.append(localDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
			stringBuilder.append("\n");
		}

		return stringBuilder.toString();
	}

	private String prepareMessageWithEducationPlan(LinkedHashSet<EducationPlan> educationPlanSet, LocalDate date) {
		if (educationPlanSet.isEmpty()) {
			return NO_EDUCATION_MESSAGE;
		}

		StringBuilder stringBuilder = new StringBuilder("Занятия на дату: ");
		stringBuilder.append(date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));

		for (EducationPlan educationPlan : educationPlanSet) {
			stringBuilder.append("\n-----------\n");
			stringBuilder.append("Время: ");
			stringBuilder.append(educationPlan.getSchedule().getInterval());

			stringBuilder.append("\nАудитория: ");
			stringBuilder.append(educationPlan.getRoom().getNumber());

			stringBuilder.append("\nПредмет: ");
			stringBuilder.append(educationPlan.getSubject().getTitle());

			stringBuilder.append("\nПреподаватель: ");
			String position = educationPlan.getTeacher().getPosition() != null ?
					educationPlan.getTeacher().getPosition() + " / " : "";
			stringBuilder.append(position);
			stringBuilder.append(educationPlan.getTeacher().getUser().getLastName());
		}

		return stringBuilder.toString();
	}

	private void startCommandReceived(TelegramUser telegramUser, String messageText) {
		String answer;
		boolean auth = false;

		if (messageText.equals("/start")) {
			answer = AUTH_MESSAGE;
		} else {
			if (!messageText.matches("\\d*")) {
				answer = STUDENT_NOT_FOUND_MESSAGE;
			} else {
				if (confirmTelegramUser(telegramUser, Long.valueOf(messageText))) {
					answer = GREETING_MESSAGE;
					auth = true;
				} else {
					answer = STUDENT_NOT_FOUND_MESSAGE;
				}
			}
		}

		sendMessage(telegramUser.getChatId(), answer, auth);

		if (auth) {
			sendMessage(telegramUser.getChatId(), GET_SCHEDULE_MESSAGE, true);
		}
	}

	private boolean confirmTelegramUser(TelegramUser telegramUser, Long idNumber) {
		Optional<Student> optionalStudent = studentService.findByIdNumber(idNumber);
		if (optionalStudent.isEmpty()) {
			return false;
		}

		telegramUserService.setStudent(telegramUser, optionalStudent.get());

		return true;
	}

	private void sendMessage(long chatId, String textToSend, Boolean auth) {
		SendMessage message = new SendMessage();
		message.setChatId(String.valueOf(chatId));
		message.setText(textToSend);

		if (auth) {
			ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
			keyboardMarkup.setResizeKeyboard(true);
			List<KeyboardRow> keyboardRows = new ArrayList<>();

			KeyboardRow row = new KeyboardRow();
			row.add(NEAREST_DATES_BUTTON);
			keyboardRows.add(row);

			keyboardMarkup.setKeyboard(keyboardRows);
			message.setReplyMarkup(keyboardMarkup);
		}

		try {
			execute(message);
		} catch (TelegramApiException e) {
			throw new RuntimeException(e);
		}
	}

}
