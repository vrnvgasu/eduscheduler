package ru.edu.eduscheduler.repository;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.eduscheduler.model.Teacher;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {

	@Query(value = """
			select t
			from Teacher t
			join fetch t.user
			where t.id not in (select tsub.id
			                   from EducationPlan ep
			                            left join Teacher tsub on tsub.id = ep.teacherId
			                   where ep.date = :localDate
			                     and ep.scheduleId = :scheduleId)
			""")
	List<Teacher> getListWithUser(@Param(value = "localDate") LocalDate localDate,
			@Param(value = "scheduleId") Long scheduleId);

}
