package ru.edu.eduscheduler.repository;

import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.eduscheduler.model.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {

	@Query(value = """
			select s from Subject s
			join s.groups g where g.id in (:groupId)
			""")
	Set<Subject> getListAvailableForGroup(@Param(value = "groupId") Long groupId);

}
