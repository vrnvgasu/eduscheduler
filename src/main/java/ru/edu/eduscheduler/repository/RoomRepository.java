package ru.edu.eduscheduler.repository;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.eduscheduler.model.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

	@Query(nativeQuery = true, value = """
			select *
			from rooms r
			where r.id not in (select r.id
			                   from education_plans
			                            left join rooms r on r.id = education_plans.room_id
			                   where date = :localDate
			                     and schedule_id = :scheduleId)
			""")
	List<Room> getListAvailable(@Param(value = "localDate") LocalDate localDate,
			@Param(value = "scheduleId") Long scheduleId);

}
