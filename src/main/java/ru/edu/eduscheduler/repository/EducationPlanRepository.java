package ru.edu.eduscheduler.repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.eduscheduler.model.EducationPlan;

@Repository
public interface EducationPlanRepository extends JpaRepository<EducationPlan, Long> {

	@Query(value = """
			select e from EducationPlan e
			join fetch e.room
			join fetch e.schedule
			join fetch e.group
			join fetch e.subject
			join fetch e.teacher t
			join fetch t.user
			where e.group.id=:groupId 
			and e.date >= :dateStart
			and e.date <= :dateEnd 
			""")
	Set<EducationPlan> getForGroup(@Param(value = "groupId") Long groupId,
			@Param(value = "dateStart") LocalDate dateStart, @Param(value = "dateEnd") LocalDate dateEnd);

	boolean existsByDateAndRoomIdAndScheduleId(LocalDate date, Long roomId, Long scheduleId);

	boolean existsByDateAndGroupIdAndScheduleId(LocalDate date, Long groupId, Long scheduleId);

	@Query(value = """
			select e from EducationPlan e
			join fetch e.room
			join fetch e.schedule
			join fetch e.group g
			join fetch e.subject
			join fetch e.teacher t
			join fetch t.user
			join g.studentSet st
			where st.id=:studentId
			and e.date = :date
			order by e.schedule.order
			""")
	LinkedHashSet<EducationPlan> getScheduleForStudent(@Param(value = "studentId") Long studentId,
			@Param(value = "date") LocalDate date);

	@Query(nativeQuery = true, value = """
			select date from education_plans e
			join groups g on g.id = e.group_id
			join students st on st.group_id = g.id
			where st.id = :studentId
			and e.date >= :date
			group by date
			order by date asc
			limit 5
			""")
	LinkedHashSet<Date> getNearestEducationDaysForStudent(@Param(value = "studentId") Long studentId,
			@Param(value = "date") LocalDate date);

	@Query(value = """
			select e from EducationPlan e
			join fetch e.schedule
			join fetch e.group
			where e.id=:educationPlanId
			""")
	Optional<EducationPlan> findByIdWithScheduleAndGroup(@Param(value = "educationPlanId") Long educationPlanId);

	@Query(value = """
			select e from EducationPlan e
			join fetch e.room
			join fetch e.schedule
			join fetch e.group
			join fetch e.subject
			join fetch e.teacher t
			join fetch t.user
			where e.room.id=:roomId 
			and e.date >= :dateStart
			and e.date <= :dateEnd 
			""")
	Set<EducationPlan> getForRoom(@Param(value = "roomId") Long roomId,
			@Param(value = "dateStart") LocalDate dateStart, @Param(value = "dateEnd") LocalDate dateEnd);

	@Query(value = """
			select e from EducationPlan e
			join fetch e.room
			join fetch e.schedule
			join fetch e.group
			join fetch e.subject
			join fetch e.teacher t
			join fetch t.user
			where e.teacher.id=:teacherId 
			and e.date >= :dateStart
			and e.date <= :dateEnd 
			""")
	Set<EducationPlan> getForTeacher(@Param(value = "teacherId") Long teacherId,
			@Param(value = "dateStart") LocalDate dateStart, @Param(value = "dateEnd") LocalDate dateEnd);

}
