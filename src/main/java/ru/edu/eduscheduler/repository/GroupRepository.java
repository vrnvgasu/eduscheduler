package ru.edu.eduscheduler.repository;

import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.edu.eduscheduler.model.Group;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {

	@Query(value = "select g from Group g join g.subjects")
	Set<Group> getListWithSubject();

}
