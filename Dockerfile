#образ взятый за основу
FROM openjdk:17
#Записываем в переменную путь до WAR файла
ARG jarFile=target/eduScheduler-1.war
#Куда мы перемещаем варник внутри контейнера
WORKDIR /opt/app
#копируем jar внутрь контейнера
COPY ${jarFile} edu.war
#открываем порт
EXPOSE 9100
#команда для запуска
ENTRYPOINT ["java", "-jar", "edu.war"]
